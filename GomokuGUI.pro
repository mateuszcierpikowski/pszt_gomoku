#-------------------------------------------------
#
# Project created by QtCreator 2016-12-11T14:03:47
#
#-------------------------------------------------

QT       += core gui

CONFIG += c++11


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GomokuGUI
TEMPLATE = app

INCLUDEPATH += $$PWD/include

SOURCES += main-gui.cpp\
        src/AlphaBeta.cpp \
        src/Board.cpp \
        src/Field.cpp \
        src/Five.cpp \
        src/Utils.cpp \
    src/MainWindow.cpp \
    src/GomokuWidget.cpp

HEADERS  += \
    include/AlphaBeta.h \
    include/Board.h \
    include/Field.h \
    include/Five.h \
    include/Utils.h \
    include/GomokuWidget.h \
    include/MainWindow.h

FORMS    += ui/mainwindow.ui
