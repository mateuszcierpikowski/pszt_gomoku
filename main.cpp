#include "AlphaBeta.h"
#include "Board.h"

Field::Marker switchMarker(const Field::Marker& marker)
{
    switch (marker)
    {
        case Field::Marker::O:
            return Field::Marker::X;
        case Field::Marker::X:
            return Field::Marker::O;
        case Field::Marker::NS:
            return Field::Marker::NS;
    }
}

int main() {

    Board board;

    board.draw();

    auto marker = Field::Marker::X;

    while (true)
    {
        uint16_t x, y;

        std::tuple<bool, Board::GameResult> result;

        do
        {
            std::cout<<"Podaj wspolrzedne ruchu"<<std::endl;
            std::cin>>x>>y;
            result = board.doMove(x, y, marker);
        }
        while (!std::get<0>(result));

        board.draw();

        if (std::get<1>(result) == Board::GameResult::AI_WIN)
        {
            std::cout<<"Komputer wygral"<<std::endl;
            break;
        }

        if (std::get<1>(result) == Board::GameResult::PLAYER_WIN)
        {
            std::cout<<"Wygrales"<<std::endl;
            break;
        }

        if (std::get<1>(result) == Board::GameResult::DRAW)
        {
            std::cout<<"Remis"<<std::endl;
            break;
        }
    }
    return 0;
}