//
// Created by mati on 07.12.16.
//

#include "AlphaBeta.h"


AlphaBeta::AlphaBeta(uint16_t timeout_ms) : timeout_ms_(timeout_ms)
{

}

void AlphaBeta::run(Possibilities &possibilities)
{
    start_time_ = std::chrono::system_clock::now();
    uint16_t move;

    for (uint i = 2;; ++i)
    {
        try
        {
            move = minMax(possibilities, i);
        }
        catch (const TimeOutException &exception)
        {
            std::cout << exception.what() << " i = " << --i << std::endl;
            break;
        }
    }
    possibilities.doRealMove(move);
    std::cout << "Solution Move: " << move << std::endl;
}

uint16_t AlphaBeta::minMax(Possibilities &possibilities, uint depth)
{
    auto ret = alphaBeta(possibilities, GameState::AI, depth, std::numeric_limits<int64_t>::min(),
                         std::numeric_limits<int64_t>::max());
    std::cout << "MinMax value: " << std::get<1>(ret) << std::endl;
    return std::get<0>(ret);
}

std::tuple<uint16_t, int64_t> AlphaBeta::alphaBeta(Possibilities &possibilities, GameState gameState,
                                                   uint depth, int64_t alpha, int64_t beta) throw(TimeOutException)
{

    if (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start_time_).count()
        >= timeout_ms_)
    {
        throw TimeOutException();
    }

    if (possibilities.isOver() || depth == 0)
    {
        return std::make_tuple(0, possibilities.getCurrentScore());
    }
    else if (gameState == GameState::AI)
    {
        --depth;
        possibilities.commitGame();
        uint16_t move = 0;
        for (uint16_t i = 0; i < possibilities.getPossibilitiesSize(); ++i)
        {
            possibilities.doHypotheticalMove(i);

            try
            {
                auto ret = alphaBeta(possibilities, changeState(gameState), depth, alpha, beta);
                possibilities.undoHypotheticalMove(i);
                if (alpha < std::get<1>(ret))
                {
                    alpha = std::get<1>(ret);
                    move = i;
                }
                if (alpha >= beta)
                {
                    possibilities.reinstateGame();
                    return std::make_tuple(std::get<0>(ret), beta);
                }
            }
            catch (const TimeOutException &exception)
            {
                possibilities.undoHypotheticalMove(i);
                possibilities.reinstateGame();
                throw exception;
            }
        }
        possibilities.reinstateGame();
        return std::make_tuple(move, alpha);
    }
    else
    {
        --depth;
        possibilities.commitGame();
        uint16_t move = 0;
        for (uint16_t i = 0; i < possibilities.getPossibilitiesSize(); ++i)
        {
            possibilities.doHypotheticalMove(i);

            try
            {
                auto ret = alphaBeta(possibilities, changeState(gameState), depth, alpha, beta);
                possibilities.undoHypotheticalMove(i);
                if (beta > std::get<1>(ret))
                {
                    beta = std::get<1>(ret);
                    move = i;
                }
                if (alpha >= beta)
                {
                    possibilities.reinstateGame();
                    return std::make_tuple(std::get<0>(ret), alpha);
                }

            }
            catch (const TimeOutException &exception)
            {
                possibilities.undoHypotheticalMove(i);
                possibilities.reinstateGame();
                throw exception;
            }
        }
        possibilities.reinstateGame();
        return std::make_tuple(move, beta);
    }
}

void AlphaBeta::setTimeout(uint16_t timeout_ms)
{
    timeout_ms_ = timeout_ms;
}