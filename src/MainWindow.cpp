#include "MainWindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->gomokuWidget, SIGNAL(gameResultChanged(Board::GameResult)), this, SLOT(onGameResultChanged(Board::GameResult)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_timeout_spinner_valueChanged(double arg1)
{
    ui->gomokuWidget->setTimeout(arg1);
}

void MainWindow::on_restart_pressed()
{
    ui->gomokuWidget->restartGame();
}

void MainWindow::onGameResultChanged(Board::GameResult result)
{
    switch (result) {
    case Board::GameResult::AI_WIN:
        ui->gameResult->setText("<font color='red'>komputer wygrał</font>");
        break;
    case Board::GameResult::PLAYER_WIN:
        ui->gameResult->setText("<font color='red'>wygrałeś</font>");
        break;
    default:
        ui->gameResult->setText("<font color='black'>w trakcie</font>");
        break;
    }
}
