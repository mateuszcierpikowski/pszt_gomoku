//
// Created by mati on 10.12.16.
//

#include "Five.h"

int64_t Five::ai_score_;
int64_t Five::player_score_;

uint8_t Five::getAiFields()
{
    return ai_fields_;
}

uint8_t Five::getPlayerFields()
{
    return player_fields_;
}

void Five::addField(Field &field)
{
    fields_.push_back(&field);
}

bool Five::setField(GameState gameState, bool real)
{
//    std::cout<<"***"<<std::endl;
//    std::cout<<int(ai_fields_)<<std::endl;
//    std::cout<<int(player_fields_)<<std::endl;
    if (gameState == GameState::AI)
    {
        ++ai_fields_;
        added_fields_.push_back(GameState::AI);

        if (player_fields_ == 0)
        {
            ai_score_ -= std::pow(10, ai_fields_ - 1);
            ai_score_ += std::pow(10, ai_fields_);

            if (ai_fields_ == 5)
            {
                if (real)
                {
                    for (auto* field: fields_)
                    {
                        field->setWinnings();
                    }
                }
                return true;
            }
        }
        else if (ai_fields_ == 1)
        {
            player_score_ -= std::pow(10, player_fields_);
            player_score_ += 1;
        }

        return false;
    }
    else
    {
        ++player_fields_;
        added_fields_.push_back(GameState::PLAYER);

        if (ai_fields_ == 0)
        {
            player_score_ -= std::pow(10, player_fields_ - 1);
            player_score_ += std::pow(10, player_fields_);

            if (player_fields_ == 5)
            {
                if (real)
                {
                    for (auto* field: fields_)
                    {
                        field->setWinnings();
                    }
                }
                return true;
            }
        }
        else if (player_fields_ == 1)
        {
            ai_score_ -= std::pow(10, ai_fields_);
            ai_score_ += 1;
        }

        return false;
    }
//    std::cout<<"+++"<<std::endl;
//    std::cout<<"Field"<<std::endl;
//    std::cout<<int(ai_fields_)<<std::endl;
//    std::cout<<int(player_fields_)<<std::endl;
}

void Five::unsetField()
{

    GameState gameState = added_fields_.back();
    added_fields_.pop_back();

    if (gameState == GameState::AI)
    {
        --ai_fields_;

        if (player_fields_ == 0)
        {
            ai_score_ -= std::pow(10, ai_fields_ + 1);
            ai_score_ += std::pow(10, ai_fields_);
        }
        else if (ai_fields_ == 0)
        {
            player_score_ += std::pow(10, player_fields_);
            player_score_ -= 1;
        }
    }
    else
    {
        --player_fields_;

        if (ai_fields_ == 0)
        {
            player_score_ -= std::pow(10, player_fields_ + 1);
            player_score_ += std::pow(10, player_fields_);
        }
        else if (player_fields_ == 0)
        {
            ai_score_ += std::pow(10, ai_fields_);
            ai_score_ -= 1;
        }
    }


//    std::cout<<"---"<<std::endl;
//    std::cout<<"Field"<<std::endl;
//    std::cout<<int(ai_fields_)<<std::endl;
//    std::cout<<int(player_fields_)<<std::endl;
}
