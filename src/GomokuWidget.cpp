#include "GomokuWidget.h"

GomokuWidget::GomokuWidget(QWidget *parent) : QWidget(parent), board_(new Board())
{
//    QPalette pal(palette());

//    // set black background
//    pal.setColor(QPalette::Background, QColor(251, 180, 0));
//    this->setAutoFillBackground(true);
//    this->setPalette(pal);
    setMouseTracking(true);
}

void GomokuWidget::paintEvent(QPaintEvent *)
{
    width_increment_ = this->width()/board_size_;
    height_increment_ = this->height()/board_size_;

    QPainter painter(this);

    QPainterPath path;
    path.addRect(QRect(0, 0, width_increment_*board_size_, height_increment_*board_size_));

    QPen pen(QColor(251, 180, 0), 1);
    painter.setPen(pen);
    painter.fillPath(path, QColor(251, 180, 0));
    painter.drawPath(path);

    auto pen_width = 2;

    painter.setPen(QPen(Qt::black, pen_width));


    for(auto x = 1; x < board_size_; ++x)
    {
        painter.drawLine(x*width_increment_, 0, x*width_increment_, height_increment_*board_size_);
    }

    for(auto y = 1; y < board_size_; ++y)
    {
        painter.drawLine(0, y*height_increment_, width_increment_*board_size_, y*height_increment_);
    }


    QPixmap pixmap;

    painter.drawLine(1, 1, width_increment_*board_size_, 1);
    painter.drawLine(1, 1, 1, height_increment_*board_size_);
    painter.drawLine(width_increment_*board_size_, 1, width_increment_*board_size_, height_increment_*board_size_);
    painter.drawLine(1, height_increment_*board_size_, width_increment_*board_size_, height_increment_*board_size_);

    auto radius = std::min(width_increment_, height_increment_)/2 - 3;

    for(auto y = 0; y <board_->getFields().size(); ++y)
    {
        for(auto x = 0; x < board_->getFields().at(y).size(); ++x)
        {
            switch (board_->getFields().at(y).at(x).getMarker()) {
            case Field::Marker::O:
                paintO(width_increment_*x, height_increment_*y, radius, painter);
                break;
            case Field::Marker::X:
                paintX(width_increment_*x, height_increment_*y, radius, painter);
                break;
            case Field::Marker::WINNING_X:
                painter.setPen(QPen(Qt::red, pen_width));
                paintX(width_increment_*x, height_increment_*y, radius, painter);
                painter.setPen(QPen(Qt::black, pen_width));
                break;
            case Field::Marker::WINNING_O:
                painter.setPen(QPen(Qt::red, pen_width));
                paintO(width_increment_*x, height_increment_*y, radius, painter);
                painter.setPen(QPen(Qt::black, pen_width));
                break;
            default:
                break;
            }
        }
    }

    if (x_ != -1 && y_ != -1)
    {
        paintX(x_, y_, radius, painter);
    }
}



void GomokuWidget::mouseMoveEvent(QMouseEvent *event)
{
//    std::cout<<event->globalX()<<std::endl;
//    std::cout<<event->globalY()<<std::endl;

//    x_ = event->pos().x();
//    y_ = event->pos().y();

//    repaint();
}

void GomokuWidget::mouseReleaseEvent(QMouseEvent *event)
{
    if (game_result_ != Board::GameResult::GAME_IN_PROGRESS)
        return;

    uint8_t x = event->pos().x() / width_increment_;
    uint8_t y = event->pos().y() / height_increment_;

    x_ = x * width_increment_;
    y_ = y * height_increment_;

    repaint();

    x_ = -1;
    y_ = -1;

    auto result = board_->doMove(x, y, Field::Marker::X);

    game_result_ = std::get<1>(result);

    emit gameResultChanged(game_result_);

    repaint();
}

void GomokuWidget::setTimeout(double timeout_s)
{
    timeout_ms_ = timeout_s * 1000;
    board_->setAlphaBetaTimeout(timeout_ms_);
    std::cout<<timeout_s*1000<<std::endl;
}

void GomokuWidget::restartGame()
{
    game_result_ = Board::GameResult::GAME_IN_PROGRESS;
    board_ = std::unique_ptr<Board>(new Board());
    board_->setAlphaBetaTimeout(timeout_ms_);
    emit gameResultChanged(game_result_);
    repaint();
}

void GomokuWidget::paintX(uint x, uint y, uint radius, QPainter& painter)
{
    x += width_increment_/2;
    y += height_increment_/2;
    painter.drawLine(QPoint(x - radius, y - radius), QPoint(x + radius, y + radius));
    painter.drawLine(QPoint(x + radius, y - radius), QPoint(x - radius, y + radius));
}

void GomokuWidget::paintO(uint x, uint y, uint radius, QPainter &painter)
{
    x += width_increment_/2;
    y += height_increment_/2;
    painter.drawEllipse(QPoint(x, y), radius, radius);
}
