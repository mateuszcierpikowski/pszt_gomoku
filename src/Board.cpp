//
// Created by mati on 10.12.16.
//

#include "Board.h"

Board::Board(): alpha_beta_(2000)
{
    for (uint16_t x = 0; x < board_size_; ++x)
    {
        for (uint16_t y = 0; y < (board_size_ - 4); ++y)
        {
            auto five = std::make_shared<Five>();
            fives_.push_back(five);
            for (uint16_t k = 0; k < 5; ++k)
            {
                fields_.at(y + k).at(x).addFive(five);
                fives_.back()->addField(fields_.at(y + k).at(x));
            }
        }
    }

    for (uint16_t x = 0; x < (board_size_ - 4); ++x)
    {
        for (uint16_t y = 0; y < board_size_; ++y)
        {
            auto five = std::make_shared<Five>();
            fives_.push_back(five);
            for (uint16_t k = 0; k < 5; ++k)
            {
                fields_.at(y).at(x + k).addFive(five);
                fives_.back()->addField(fields_.at(y).at(x + k));
            }
        }
    }

    for (uint16_t x = 0; x < (board_size_ - 4); ++x)
    {
        for (uint16_t y = 0; y < (board_size_ - 4); ++y)
        {
            auto five = std::make_shared<Five>();
            fives_.push_back(five);
            for (uint16_t k = 0; k < 5; ++k)
            {
                fields_.at(y + k).at(x + k).addFive(five);
                fives_.back()->addField(fields_.at(y + k).at(x + k));
            }
        }
    }

    for (uint16_t x = (board_size_ - 1); x > 3; --x)
    {
        for (uint16_t y = 0; y < (board_size_ - 4); ++y)
        {
            auto five = std::make_shared<Five>();
            fives_.push_back(five);
            for (uint16_t k = 0; k < 5; ++k)
            {
                fields_.at(y + k).at(x - k).addFive(five);
                fives_.back()->addField(fields_.at(y + k).at(x - k));
            }
        }
    }

    Five::player_score_ = fives_.size();
    Five::ai_score_ = fives_.size();
}

void Board::draw()
{
    std::cout << std::setw(4) << " ";
    for (uint16_t x = 0; x < board_size_; ++x)
    {
        std::cout << std::setw(4) << x;
    }
    std::cout << std::endl;
    for (uint16_t x = 0; x < board_size_; ++x)
    {
        std::cout << std::setw(4) << x;
        for (uint16_t y = 0; y < board_size_; ++y)
        {
            std::cout << std::setw(4) << fields_.at(y).at(x).getMarkerString();
        }
        std::cout << std::endl;
    }
}

std::tuple<bool, Board::GameResult> Board::doMove(uint8_t x, uint8_t y, Field::Marker marker)
{
    if (fields_.at(y).at(x).getMarker() == Field::Marker::X || fields_.at(y).at(x).getMarker() == Field::Marker::O)
    {
        return std::make_tuple(false, Board::GameResult::GAME_IN_PROGRESS);
    }

    fields_.at(y).at(x).setMarker(std::move(marker));
    moves_.insert({x, y});

    calculatedPossibilities(x, y);

    if (fields_.at(y).at(x).calculatedScore(GameState::PLAYER, true))
    {
        return std::make_tuple(true, Board::GameResult::PLAYER_WIN);
    }

    if (possibilities_.size() == 0)
    {
        return std::make_tuple(true, Board::GameResult::DRAW);
    }

//    std::cout<<"----------"<<std::endl;
//    std::cout<<Five::ai_score_<<std::endl;
//    std::cout<<Five::player_score_<<std::endl;

    alpha_beta_.run(*this);

    if (ai_win_)
    {
        return std::make_tuple(true, Board::GameResult::AI_WIN);
    }

    if (possibilities_.size() == 0)
    {
        return std::make_tuple(true, Board::GameResult::DRAW);
    }

    return std::make_tuple(true, Board::GameResult::GAME_IN_PROGRESS);
}

void Board::calculatedPossibilities(uint8_t x, uint8_t y)
{
    for (int8_t i = int8_t(x) - int8_t(1); i <= int8_t(x) + int8_t(1); ++i)
    {
        for (int8_t j = int8_t(y) - int8_t(1); j <= int8_t(y) + int8_t(1); ++j)
        {
            if (i < 0 || j < 0 || i > board_size_ - 1 || j > board_size_ - 1)
            {
                continue;
            }
            possibilities_.insert(Move(static_cast<uint8_t>(i), static_cast<uint8_t>(j)));
        }
    }

    for (const Move &move: moves_)
    {
        possibilities_.erase(move);
    }
}

int64_t Board::getCurrentScore()
{
    return Five::ai_score_ - Five::player_score_;
}

uint16_t Board::getPossibilitiesSize()
{
    return static_cast<uint16_t>(possibilities_.size());
}

void Board::doHypotheticalMove(uint16_t number_of_move)
{
    auto it = std::next(possibilities_.begin(), number_of_move);

//    std::cout<<"---------------"<<std::endl;
//    std::cout<<"Hypothetical move"<<std::endl;
//    std::cout<<*it<<std::endl;

    moves_.insert(*it);
    calculatedPossibilities(it->x_, it->y_);

    if (commited_possibilities_.size() % 2 == 0)
    {
        is_over_ = fields_.at(it->y_).at(it->x_).calculatedScore(GameState::PLAYER, false);
    } else
    {
        is_over_ = fields_.at(it->y_).at(it->x_).calculatedScore(GameState::AI, false);
    }

//    std::cout<<std::endl;
//    std::cout<<"---------- move "<<int(number_of_move)<<std::endl;
//    std::cout<<"AI \t"<<Five::ai_score_<<std::endl;
//    std::cout<<"PLAYER \t"<<Five::player_score_<<std::endl;
}

void Board::undoHypotheticalMove(uint16_t number_of_move)
{
    possibilities_ = commited_possibilities_.back();

    auto it = std::next(possibilities_.begin(), number_of_move);
    moves_.erase({it->x_, it->y_});
    fields_.at(it->y_).at(it->x_).unsetFives();

//    std::cout<<"**************** move "<<int(number_of_move)<<std::endl;
//    std::cout<<"Undo move"<<std::endl;
//    std::cout<<*it<<std::endl;
//    std::cout<<"AI \t"<<Five::ai_score_<<std::endl;
//    std::cout<<"PLAYER \t"<<Five::player_score_<<std::endl;
//    std::cout<<std::endl;

    is_over_ = false;
}

void Board::doRealMove(uint16_t number_of_move)
{
    auto it = std::next(possibilities_.begin(), number_of_move);
    fields_.at(it->y_).at(it->x_).setMarker(Field::Marker::O);
    moves_.insert({it->x_, it->y_});

    if (fields_.at(it->y_).at(it->x_).calculatedScore(GameState::AI, true))
    {
        ai_win_ = true;
        return;
    }
    calculatedPossibilities(it->x_, it->y_);

    for (const Move &move: possibilities_)
    {
        fields_.at(move.y_).at(move.x_).setMarker(Field::Marker::DEBUG);
    }
}

bool Board::isOver()
{
    return is_over_;
}

void Board::commitGame()
{
    commited_possibilities_.push_back(possibilities_);
}

void Board::reinstateGame()
{
    possibilities_ = commited_possibilities_.back();
    commited_possibilities_.pop_back();
    is_over_ = false;
}

void Board::setAlphaBetaTimeout(uint16_t timeout_ms)
{
    alpha_beta_.setTimeout(timeout_ms);
}

const std::array<std::array<Field, board_size_>, board_size_>& Board::getFields()
{
    return fields_;
}
