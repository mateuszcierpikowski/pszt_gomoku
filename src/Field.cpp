//
// Created by mati on 10.12.16.
//

#include "Field.h"

void Field::addFive(const PFive &five)
{
    fives_.push_back(five);
}

int Field::getFiveSize()
{
    return fives_.size();
}

const Field::Marker &Field::getMarker() const
{
    return marker_;
}

std::string Field::getMarkerString()
{
    switch (marker_)
    {
        case Field::Marker::O:
            return "O";
        case Field::Marker::X:
            return "X";
        case Field::Marker::NS:
            return "-";
        case Field::Marker::DEBUG:
            return "*";
    }
}

void Field::setMarker(Marker marker)
{
    marker_ = std::move(marker);
}

bool Field::calculatedScore(GameState gameState, bool real)
{
    bool ret = false;
    for (const auto &five: fives_)
    {
        if (five.lock()->setField(gameState, real))
        {
            ret = true;
        }
    }
    return ret;
}

void Field::unsetFives()
{
    for (const auto &five: fives_)
    {
        five.lock()->unsetField();
    }
}

void Field::setWinnings()
{
    if (marker_ == Marker::O)
    {
        marker_ = Marker::WINNING_O;
    }
    else if (marker_ == Marker::X)
    {
        marker_ = Marker::WINNING_X;
    }
}
