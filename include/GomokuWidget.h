#ifndef GOMOKUWIDGET_H
#define GOMOKUWIDGET_H

#include <QWidget>
#include <QPainter>
#include <QPixmap>
#include <QMouseEvent>

#include "Board.h"

#include <iostream>

class GomokuWidget : public QWidget
{
    Q_OBJECT
public:
    explicit GomokuWidget(QWidget *parent = 0);

protected:
    void paintEvent(QPaintEvent*);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
signals:
    void gameResultChanged(Board::GameResult result);
public slots:
    void setTimeout(double timeout_s);
    void restartGame();

private:
    std::unique_ptr<Board> board_;

    uint width_increment_;
    uint height_increment_;

    uint timeout_ms_ = 2000;

    int x_ = -1;
    int y_ = -1;

    Board::GameResult game_result_{Board::GameResult::GAME_IN_PROGRESS};

    void paintX(uint x, uint y, uint radius, QPainter& painter);
    void paintO(uint x, uint y, uint radius, QPainter& painter);
};

#endif // GOMOKUWIDGET_H
