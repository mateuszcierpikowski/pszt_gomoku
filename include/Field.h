//
// Created by mati on 10.12.16.
//

#ifndef PSZT_TEMP_FIELD_H
#define PSZT_TEMP_FIELD_H

#include <memory>
#include <vector>

#include "Five.h"

class Five;

using PWFive = std::weak_ptr<Five>;
using PFive = std::shared_ptr<Five>;

class Field {
public:
    enum class Marker{
        X, O, NS, DEBUG, WINNING_X, WINNING_O
    };
    void addFive(const PFive& five);
    int getFiveSize();
    const Marker& getMarker() const;
    std::string getMarkerString();
    void setMarker(Marker marker);
    bool calculatedScore(GameState gameState, bool real);
    void unsetFives();
    void setWinnings();
private:
    std::vector<PWFive> fives_;
    Marker marker_{Marker::NS};
};


#endif //PSZT_TEMP_FIELD_H
