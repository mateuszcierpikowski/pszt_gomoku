#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "Board.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_timeout_spinner_valueChanged(double arg1);
    void on_restart_pressed();
    void onGameResultChanged(Board::GameResult result);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
