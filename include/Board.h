//
// Created by mati on 10.12.16.
//

#ifndef PSZT_TEMP_BOARD_H
#define PSZT_TEMP_BOARD_H

#include <future>
#include <iomanip>
#include <iostream>
#include <set>
#include <vector>

#include "Five.h"
#include "Field.h"
#include "AlphaBeta.h"

const static uint8_t board_size_ = 25;

class Board : public Possibilities {
public:
    enum class GameResult {
        AI_WIN, PLAYER_WIN, DRAW, GAME_IN_PROGRESS
    };

    struct Move {
        Move(uint8_t x, uint8_t y) :
                x_(x),y_(y)
        {}

        uint8_t x_;
        uint8_t y_;

        bool operator==(const Move& rhs) const
        {
            return this->x_ == rhs.x_ && this->y_ == rhs.y_;
        }
        bool operator<(const Move& rhs) const
        {
            return std::tie(this->x_, this->y_) < std::tie(rhs.x_, rhs.y_);
        }
        friend std::ostream& operator<<(std::ostream& ostream, const Move& rhs)
        {
            ostream<<"x "<<int(rhs.x_)<<"\t"<<"y "<<int(rhs.y_);
            return ostream;
        }
    };

    Board();
    void draw();
    std::tuple<bool, GameResult> doMove(uint8_t x, uint8_t y, Field::Marker marker);
    void setAlphaBetaTimeout(uint16_t timeout_ms);
    const std::array<std::array<Field, board_size_>, board_size_>& getFields();

    virtual int64_t getCurrentScore();
    virtual uint16_t getPossibilitiesSize();
    virtual void doHypotheticalMove(uint16_t number_of_move);
    virtual void undoHypotheticalMove(uint16_t number_of_move);
    virtual bool isOver();
    virtual void doRealMove(uint16_t number_of_move);
    virtual void commitGame();
    virtual void reinstateGame();

private:
    std::vector<std::shared_ptr<Five>> fives_;
    std::array<std::array<Field, board_size_>, board_size_> fields_;
    AlphaBeta alpha_beta_;
    std::set<Move> possibilities_;
    std::set<Move> moves_;
    std::vector<std::set<Move>> commited_possibilities_;
    bool is_over_{false};
    bool ai_win_{false};

    void calculatedPossibilities(uint8_t x, uint8_t y);
};


#endif //PSZT_TEMP_BOARD_H
