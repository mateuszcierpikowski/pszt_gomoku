//
// Created by mati on 10.12.16.
//

#ifndef PSZT_TEMP_UTILS_H
#define PSZT_TEMP_UTILS_H

enum class GameState {
    AI = 0, PLAYER = 1
};

GameState changeState(GameState state);


#endif //PSZT_TEMP_UTILS_H
