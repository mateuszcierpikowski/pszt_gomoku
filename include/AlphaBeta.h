//
// Created by mati on 07.12.16.
//

#ifndef PSZT_TEMP_ALPHABETA_H
#define PSZT_TEMP_ALPHABETA_H

#include <iostream>
#include <vector>
#include <array>
#include <random>
#include <limits.h>
#include <algorithm>
#include <chrono>
#include <exception>
#include <tuple>

#include <Utils.h>

class TimeOutException: public std::exception
{
public:
    virtual const char* what() const throw()
    {
        return "Timeout";
    }
};

class Possibilities {
public:
    virtual ~Possibilities() = default;
    virtual int64_t getCurrentScore() = 0;
    virtual uint16_t getPossibilitiesSize() = 0;
    virtual void doHypotheticalMove(uint16_t number_of_move) = 0;
    virtual void undoHypotheticalMove(uint16_t number_of_move) = 0;
    virtual void doRealMove(uint16_t number_of_move) = 0;
    virtual bool isOver() = 0;
    virtual void commitGame() = 0;
    virtual void reinstateGame() = 0;
};

class AlphaBeta {
public:
    AlphaBeta(uint16_t timeout_ms);
    void run(Possibilities& possibilities);
    void setTimeout(uint16_t timeout_ms);
private:
    uint16_t minMax(Possibilities& possibilities, uint depth);
    std::tuple<uint16_t, int64_t> alphaBeta(Possibilities& possibilities, GameState gameState,
                       uint depth, int64_t alpha, int64_t beta) throw(TimeOutException);
    std::chrono::system_clock::time_point start_time_;
    uint16_t timeout_ms_;
};



#endif //PSZT_TEMP_ALPHABETA_H
