//
// Created by mati on 10.12.16.
//

#ifndef PSZT_TEMP_FIVE_H
#define PSZT_TEMP_FIVE_H

#include <future>
#include <cmath>
#include <iostream>
#include <vector>

#include "Utils.h"
#include "Field.h"

class Field;

class Five {
public:
    bool setField(GameState gameState, bool real);
    void unsetField();
    uint8_t getAiFields();
    uint8_t getPlayerFields();
    void addField(Field &field);

    static int64_t ai_score_;
    static int64_t player_score_;

private:
    uint8_t ai_fields_{0};
    uint8_t player_fields_{0};
    std::vector<GameState> added_fields_;
    std::vector<Field*> fields_;
};


#endif //PSZT_TEMP_FIVE_H
