//
// Created by mati on 11.12.16.
//

#include <gtest/gtest.h>

#include "Five.h"

TEST(Five, flow)
{
    Five test_five;

    Five::ai_score_ = 1;
    Five::player_score_ = 1;

    test_five.setField(GameState::AI, false);

    ASSERT_EQ(10, Five::ai_score_);
    ASSERT_EQ(1, Five::player_score_);

    test_five.setField(GameState::AI, false);

    ASSERT_EQ(100, Five::ai_score_);
    ASSERT_EQ(1, Five::player_score_);

    test_five.setField(GameState::PLAYER, false);

    ASSERT_EQ(1, Five::ai_score_);
    ASSERT_EQ(1, Five::player_score_);

    test_five.setField(GameState::PLAYER, false);

    ASSERT_EQ(1, Five::ai_score_);
    ASSERT_EQ(1, Five::player_score_);

    test_five.setField(GameState::AI, false);

    ASSERT_EQ(1, Five::ai_score_);
    ASSERT_EQ(1, Five::player_score_);

    test_five.unsetField();

    ASSERT_EQ(1, Five::ai_score_);
    ASSERT_EQ(1, Five::player_score_);

    test_five.unsetField();

    ASSERT_EQ(1, Five::ai_score_);
    ASSERT_EQ(1, Five::player_score_);

    test_five.unsetField();

    ASSERT_EQ(100, Five::ai_score_);
    ASSERT_EQ(1, Five::player_score_);

    test_five.unsetField();

    ASSERT_EQ(10, Five::ai_score_);
    ASSERT_EQ(1, Five::player_score_);

    test_five.unsetField();

    ASSERT_EQ(1, Five::ai_score_);
    ASSERT_EQ(1, Five::player_score_);

}
